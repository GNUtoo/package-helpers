#!/bin/sh
#
#    Copyright (C) 2008-2020 Ruben Rodriguez <ruben@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=4
COMPONENT=main

. ./config

cat << EOF > data/templates/Trisquel.mirrors
#LOC:CA
https://mirror.freemirror.org/trisquel-packages/
#LOC:FR
https://archive.trisquel.info/trisquel/
#LOC:US
https://mirror.fsf.org/trisquel/
http://mirrors.knoesis.org/trisquel/packages
https://mirrors.ocf.berkeley.edu/trisquel/
#LOC:IN
http://in.archive.trisquel.info/trisquel/
#LOC:EC
https://mirror.cedia.org.ec/trisquel.packages
#LOC:RO
http://mirrors.serverhost.ro/trisquel/packages
#LOC:CN
https://mirrors.ustc.edu.cn/trisquel
#LOC:ES
https://ftp.caliu.cat/pub/distribucions/trisquel/packages
https://mirror.librelabucm.org/trisquel/
#LOC:TW
https://ftp.yzu.edu.tw/Linux/trisquel.packages/
#LOC:SE
https://mirror.linux.pizza/trisquel/packages/
https://ftp.acc.umu.se/mirror/trisquel/packages/
#LOC:HU
https://quantum-mirror.hu/mirrors/pub/trisquel/packages/
EOF

cat << EOF > data/templates/Trisquel.info.in
_ChangelogURI: http://packages.trisquel.info/changelogs/pool/%s/%s/%s/%s_%s/changelog

Suite: $CODENAME
RepositoryType: deb
BaseURI: https://archive.trisquel.info/trisquel/
MatchURI: archive.trisquel.info/trisquel
MirrorsFile: Trisquel.mirrors
_Description: Trisquel $REVISION '$CODENAME'
Component: main
_CompDescription: Officially supported
_CompDescriptionLong: Officially supported

Suite: $CODENAME-security
ParentSuite: $CODENAME
RepositoryType: deb
BaseURI: https://archive.trisquel.info/trisquel/
MatchURI: archive.trisquel.info/trisquel
_Description: Important security updates

Suite: $CODENAME-updates
ParentSuite: $CODENAME
RepositoryType: deb
_Description: Recommended updates

Suite: $CODENAME-backports
ParentSuite: $CODENAME
RepositoryType: deb
_Description: Unsupported updates
EOF

# Fix locale issues that break ubiquity install
cat << EOF | patch -p1
diff -ru source.orig/apt/progress/base.py source/apt/progress/base.py
--- source.orig/apt/progress/base.py	2015-11-26 11:33:29.000000000 -0500
+++ source/apt/progress/base.py	2017-01-07 14:33:29.879872269 -0500
@@ -251,7 +251,8 @@
             if match:
                 self.conffile(match.group(1), match.group(2))
         elif status == "pmstatus":
-            # FIXME: Float comparison
+            if isinstance(percent, str):
+                percent = float(percent.replace(',', '.'))
             if float(percent) != self.percent or status_str != self.status:
                 self.status_change(pkgname, float(percent), status_str.strip())
                 self.percent = float(percent)
EOF

export DEB_BUILD_OPTIONS=nocheck

changelog "Compiled for Trisquel"

compile

