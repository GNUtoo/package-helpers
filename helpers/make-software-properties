#!/bin/sh
#
#    Copyright (C) 2011-2020  Ruben Rodriguez <ruben@trisquel.info>
#    Copyright (C) 2011  Carlos Pais <freemind@lavabit.com>
#    Copyright (C) 2019 Mason Hock <mason@masonhock.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=3

. ./config

cat << EOF | patch -p1
--- source.orig/softwareproperties/SoftwareProperties.py        2014-04-14 17:10:58.000000000 +0200
+++ source/softwareproperties/SoftwareProperties.py     2014-05-18 03:02:05.309290804 +0200
@@ -725,8 +725,17 @@
     site is in whitelist or the shortcut implementer adds it.
     """
 
+    codenames = {}
+    codenames["etiona"] = "bionic" 
+    codenames["flidas"] = "xenial" 
+    codenames["belenos"] = "trusty" 
+    codenames["toutatis"] = "precise" 
+    codenames["brigantia"] = "oneiric" 
+    codenames["dagda"] = "natty"
+    codenames["slaine"] = "maverick"
+    codenames["taranis"] = "lucid"
+    codenames["awen"] = "karmic" 
+    (deb_line, file) = shortcut.expand(codename=codenames[self.distro.codename])
-    (deb_line, file) = shortcut.expand(
-        codename=self.distro.codename, distro=self.distro.id.lower())
     deb_line = self.expand_http_line(deb_line)
     debsrc_entry_type = 'deb-src' if enable_source_code else '# deb-src'
     debsrc_line = debsrc_entry_type + deb_line[3:]
EOF

sed '/UbuntuDrivers/d' -i softwareproperties/gtk/SoftwarePropertiesGtk.py
sed '/vbox_drivers/,+1 s/True/False/' -i data/gtkbuilder/main.ui

replace Kubuntu Triskel .
replace Ubuntu Trisquel .
replace ftpmaster@ubuntu.com trisquel-devel@listas.trisquel.info .
replace cdimage@ubuntu.com trisquel-devel@listas.trisquel.info .
replace TrisquelDistribution UbuntuDistribution .
sed '/NoDisplay/d; s/Trisquel/Ubuntu/' -i data/software-properties-gtk.desktop.in

sed '/ubuntu-drivers-common/d' -i debian/control
sed '/software-properties-drivers/d' -i debian/software-properties-gtk.install setup.cfg

replace "Trisquel-Gettext" "Ubuntu-Gettext" .

sed '/test_suite/d' -i setup.py
rm -rf tests

sed '/gir1.2-snapd-1/d' -i debian/control
sed '/self.init_livepatch/d' -i softwareproperties/gtk/SoftwarePropertiesGtk.py
patch -p0 < $DATA/remove_livepatch_from_gtk_ui.patch
patch -p1 < $DATA/disable-livepatch.patch
sed 's/status = self.get_status()/status = Snapd.SnapStatus.UNKNOWN/' -i softwareproperties/LivepatchSnap.py

rm data/software-properties-drivers.desktop.in
sed -i '/software-properties-drivers/d' po/POTFILES.in

changelog "Removed Ubuntu-specifc tests, Livepatch, Snap"

compile

